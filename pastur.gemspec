$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "pastur/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "pastur"
  s.version     = Pastur::VERSION
  s.authors     = ["Josh Greenberg"]
  s.email       = ["joshgreenberg91@gmail.com"]
  s.homepage    = "https://bitbucket.org/josh_greenberg/pastur"
  s.summary     = "PAge-Specific TURbolinks"
  s.description = "Super simple pattern to effectively organize page-specific JS within Rails for use with Turbolinks"
  s.license     = "MIT"

  s.files = Dir["{lib,vendor}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency 'turbolinks', '~> 5'
  s.add_dependency 'jquery-rails'
  s.add_dependency 'jquery-turbolinks'
end
