window.App || (window.App = {});

App.Event = (function() {
  function Event(event1, selector1, code1) {
    this.event = event1;
    this.selector = selector1;
    this.code = code1;
  }

  return Event;

})();

App.event = function(event, selector, code) {
  if (!code) {
    code = selector;
    selector = null;
  }
  return new App.Event(event, selector, code);
};

App.page = function(selector, events) {
  return events.forEach(function(e) {
    return $(document).on(e.event, e.selector, function(event) {
      if ($(selector).length > 0) {
        return e.code(event);
      }
    });
  });
};
